require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  setup do
    @article = articles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:articles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create article" do
    assert_difference('Article.count') do
      post :create, article: { can_purchase: @article.can_purchase, can_sell: @article.can_sell, category_id: @article.category_id, cost_price: @article.cost_price, description: @article.description, gencode: @article.gencode, picture: @article.picture, public_price: @article.public_price, purchase_price: @article.purchase_price, purchase_vat_id: @article.purchase_vat_id, ref: @article.ref, sale_vat_id: @article.sale_vat_id, serial_number: @article.serial_number, supplier_id: @article.supplier_id, title: @article.title, warranty: @article.warranty }
    end

    assert_redirected_to article_path(assigns(:article))
  end

  test "should show article" do
    get :show, id: @article
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @article
    assert_response :success
  end

  test "should update article" do
    patch :update, id: @article, article: { can_purchase: @article.can_purchase, can_sell: @article.can_sell, category_id: @article.category_id, cost_price: @article.cost_price, description: @article.description, gencode: @article.gencode, picture: @article.picture, public_price: @article.public_price, purchase_price: @article.purchase_price, purchase_vat_id: @article.purchase_vat_id, ref: @article.ref, sale_vat_id: @article.sale_vat_id, serial_number: @article.serial_number, supplier_id: @article.supplier_id, title: @article.title, warranty: @article.warranty }
    assert_redirected_to article_path(assigns(:article))
  end

  test "should destroy article" do
    assert_difference('Article.count', -1) do
      delete :destroy, id: @article
    end

    assert_redirected_to articles_path
  end
end
