class Partner < ActiveRecord::Base
  has_many :projects
  has_and_belongs_to_many :interventions
  has_attached_file :logo, styles: { medium: "300x300>", thumb: "100x100>", mini: "50x50" }, default_url: "/assets/nologo_:style.png"
  validates_attachment_content_type :logo, :content_type => /\Aimage\/.*\Z/

  def interventions
    return Intervention.joins(:project).where("projects.partner_id=?", self.id)
  end

end
