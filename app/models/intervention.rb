class Intervention < ActiveRecord::Base
  validates :partner_id, :presence => true
  validates :user_id, :presence => true
  belongs_to :project
  belongs_to :user
  has_and_belongs_to_many :users
  has_and_belongs_to_many :articles, join_table: :interventions_lines
	has_many :interventions_lines
  has_attached_file :signature, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :signature, :content_type => /\Aimage\/.*\Z/

  def partner
    return self.project ? self.project.partner : nil
  end
end
