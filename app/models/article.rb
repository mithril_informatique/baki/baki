class Article < ActiveRecord::Base
  has_attached_file :picture, styles: { medium: "300x300>", thumb: "100x100>", mini: "50x50" }, default_url: "/assets/nologo_:style.png"
  validates_attachment_content_type :picture, :content_type => /\Aimage\/.*\Z/
  belongs_to :supplier, :class_name => "Partner"
  belongs_to :category
  belongs_to :sale_vat, :class_name => "Vat"
  belongs_to :purchase_vat, :class_name => "Vat"
  has_and_belongs_to_many :interventions, join_table: :interventions_lines
  validates :title, :category_id, :purchase_vat, :sale_vat, :presence => true
end
