class Category < ActiveRecord::Base
  belongs_to :sale_vat, :class_name => "Vat"
  belongs_to :purchase_vat, :class_name => "Vat"
end
