class InterventionsLine < ActiveRecord::Base
  belongs_to :intervention
  belongs_to :article
  validates :quantity, :unit_price, :sale_vat_id, :presence => true
end
