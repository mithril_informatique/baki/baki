class Project < ActiveRecord::Base
  validates :partner_id, :presence => true
  belongs_to :partner
  has_many :inteventions

  def full_title
    return "#{self.partner.name} - #{self.title}"
  end

  def duree_interventions date=nil
    interventions = Intervention.where(project_id: self.id)
    inteventions = inteventions.where("intervention_date >= ?", date) if date
    return interventions.sum(:duration)
  end
end
