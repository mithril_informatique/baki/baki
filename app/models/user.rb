class User < ActiveRecord::Base
  acts_as_authentic do |c|
		c.crypto_provider = Authlogic::CryptoProviders::Sha512
  end
  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>", mini: "50x50" }, default_url: "/assets/nologo_:style.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates :email, :name, :presence => true
  has_many :interventions
end
