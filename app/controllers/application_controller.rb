class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  helper_method :current_user_session, :current_user
  before_action :only_logged_users

  def only_logged_users
    unless current_user or valid_token
      redirect_to controller: 'user_sessions', action: 'login'
    end
  end

  def valid_token
    if params["user_credentials"]
      user = get_user_from_token params["user_credentials"]
      return user
    else
      return false
    end
  end

  private
    def current_user_session
      return @current_user_session if defined?(@current_user_session)
      @current_user_session = UserSession.find
    end

    def current_user
      return @current_user if defined?(@current_user)
      @current_user = current_user_session && current_user_session.user
    end

    def get_user_from_token token
      return User.where(single_access_token: token).first
    end
end
