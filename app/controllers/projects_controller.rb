class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  before_action :get_partners, only: [:new, :edit]

  # GET /projects
  # GET /projects.json
  def index
    @projects = Project.includes(:partner).order("partners.name", "projects.title")
    @fullTitle = true
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    @interventions = Intervention.where(project_id: @project.id)
    @duree_intervention = @project.duree_interventions
    deg = nil
    if @duree_intervention && @project.max_hours
      deg = @duree_intervention*360/@project.max_hours
      @pourc = @duree_intervention*100/@project.max_hours
    else
      deg = 360
    end
    @deg_right = deg>=180 ? 180 : deg
    @deg_left = deg>180 ? (deg<360 ? deg-180 : 180) : 0
  end

  # GET /projects/new
  def new
    @project = Project.new
    @partners = @partners.where(id: params[:partner_id]) if params[:partner_id]
  end

  # GET /projects/1/edit
  def edit
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to @project, notice: 'Le projet a bien été créé.' }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Le projet a été mis à jour.' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Le projet a été supprimé.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    def get_partners
      @partners = Partner.order(:name)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:title, :partner_id, :echance, :description, :facturable, :max_hours)
    end
end
