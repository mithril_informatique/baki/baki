class UserSessionsController < ApplicationController
  skip_before_action :only_logged_users, only: [:login, :new, :create, :get_token]
  skip_before_action :verify_authenticity_token, only: [:get_token]

  def new
    @user_session = UserSession.new
  end

  def create
    @user_session = UserSession.new(params[:user_session])
    respond_to do |format|
      if @user_session.save
        format.html { redirect_to partners_path, notice: "Bienvenu #{current_user.name}" }
        format.json { render json: { success: true, "id": current_user.id, "token": current_user.single_access_token} }
      else
        format.html { redirect_to :back, notice: "Mauvais courriel ou mot de de passe" }
        format.json { render json: { success: false} }
      end
    end
  end

  def destroy
    current_user_session.destroy
    redirect_to "/login", notice: "Vous avez été déconnecté"
  end

  def login

  end

  def get_token
    @user_session = UserSession.new("email": params[:email], "password": params[:password])
    if @user_session.save

      render json: {"logged":1, "id": current_user.id, "token": current_user.single_access_token, "name": current_user.name }
    else
      render json: {"logged":0}
    end
  end
end
