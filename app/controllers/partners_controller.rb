class PartnersController < ApplicationController
  before_action :set_partner, only: [:show, :edit, :update, :destroy, :interventions, :projects]

  # GET /partners
  # GET /partners.json
  def index
    @partners = Partner.order(:name)
		@partners = @partners.where(supplier: params['suppliers'].to_i) if params['suppliers']
		@partners = @partners.where(customer: params['customers'].to_i) if params['customers']
  end

  # GET /partners/1
  # GET /partners/1.json
  def show
    @interventions = Intervention.joins(:project).where("projects.partner_id=?", @partner.id).order("intervention_date DESC")
    @projects = Project.where(partner_id: @partner.id)
  end

  # GET /partners/new
  def new
    @partner = Partner.new
  end

  # GET /partners/1/edit
  def edit
  end

  # POST /partners
  # POST /partners.json
  def create
    @partner = Partner.new(partner_params)

    respond_to do |format|
      if @partner.save
        format.html { redirect_to @partner, notice: 'Le partenaire a bien été créé.' }
        format.json { render :show, status: :created, location: @partner }
      else
        format.html { render :new }
        format.json { render json: @partner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /partnesr/1
  # PATCH/PUT /partners/1.json
  def update
    respond_to do |format|
      if @partner.update(partner_params)
        format.html { redirect_to @partner, notice: 'Le partenaire a bien été modifié.' }
        format.json { render :show, status: :ok, location: @partner }
      else
        format.html { render :edit }
        format.json { render json: @partner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /partners/1
  # DELETE /partners/1.json
  def destroy
    @partner.destroy
    respond_to do |format|
      format.html { redirect_to partners_url, notice: 'Le partenaire a été supprimé.' }
      format.json { head :no_content }
    end
  end

  # GET /partners/1/interventions.json
  def interventions
    @interventions = Intervention.where(partner_id: @partner.id).order("intervention_date DESC")
  end

	# GET /partners/1/projects.json
  def projects
    @projects = Project.where(partner_id: @partner.id)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_partner
      @partner = Partner.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def partner_params
      params.require(:partner).permit(:name, :email, :tel, :mobile, :fax, :address, :lat, :long, :notes, :logo, :supplier, :customer)
    end
end
