class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :set_suppliers, only: [:edit, :new, :create]
  before_action :set_categories, only: [:edit, :new, :create]
  before_action :set_vats, only: [:edit, :new, :create]
  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.order(:title)

  end

  # GET /articles/1
  # GET /articles/1.json
  def show
  end

  # GET /articles/new
  def new
    @article = Article.new
    @article.can_sell = true
    @article.can_purchase = true
    @frais = nil
    @marge_percent = nil
    @marge = nil
  end

  # GET /articles/1/edit
  def edit
    @frais = @article.cost_price.to_f-@article.purchase_price.to_f
    @marge_percent = @article.public_price && @article.cost_price ? ((@article.public_price-@article.cost_price)*100/@article.cost_price).round(2) : nil
    @marge = @article.public_price && @article.cost_price ? @article.public_price-@article.cost_price : nil
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)

    respond_to do |format|
      if @article.save
        format.html { redirect_to articles_path, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to articles_path, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:purchase_price, :cost_price, :public_price, :supplier_id, :trackable, :category_id, :gencode, :description, :title, :can_sell, :can_purchase, :ref, :warranty, :purchase_vat_id, :sale_vat_id, :picture, :supplier_stock)
    end

    def set_suppliers
      @suppliers = Partner.where(supplier: true).order(:name)
    end
    def set_categories
      @categories = Category.order(:title)
    end
    def set_vats
      @vats = Vat.order(:title)
    end
    @vats
end
