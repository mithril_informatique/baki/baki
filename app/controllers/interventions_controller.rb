class InterventionsController < ApplicationController
  before_action :set_intervention, only: [:show, :edit, :update, :destroy]
  before_action :get_partners, only: [:edit, :new, :create]
  skip_before_action :verify_authenticity_token, only: [:create, :update]

  # GET /interventions
  # GET /interventions.json
  def index
    @interventions = Intervention.order("intervention_date DESC")
    @titre = "Interventions"
    @accounted = !params["accounted"].nil?
    if params["accounted"]=="1"
      @titre += " comptabilisées"
      @interventions = @Interventions.where(compta: true)
    elsif params["accounted"]=="0"
      @titre += " à comptabiliser"
      @interventions = @interventions.where(accounted: false, billable: true).where.not(duration: nil)
    end
    @tempsTotal = 0
    @interventions.each do |intervention|
      @tempsTotal += intervention.duration.to_f
    end
  end

  # GET /interventions/1
  # GET /interventions/1.json
  def show
  end

  # GET /interventions/new
  def new
    @intervention = Intervention.new
  end

  # GET /interventions/1/edit
  def edit
  end

  # POST /interventions
  # POST /interventions.json
  def create
    @intervention = Intervention.new(intervention_params)
    user_id = params["user_credentials"] ? get_user_from_token(params["user_credentials"]).id : @current_user.id
    @intervention.user_id = user_id
    respond_to do |format|
      if @intervention.save
        format.html { redirect_to interventions_path, notice: "L'intervention a été créé." }
        format.json { render :show, status: :created, location: @intervention }
      else
        format.html { render :new }
        format.json { render json: @intervention.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /interventions/1
  # PATCH/PUT /interventions/1.json
  def update
    respond_to do |format|
      if @intervention.update(intervention_params)
        format.html { redirect_to interventions_path, notice: "L'intervention a été modifiée." }
        format.json { render :show, status: :ok, location: @intervention }
      else
        format.html { render :edit }
        format.json { render json: @intervention.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /interventions/1
  # DELETE /interventions/1.json
  def destroy
    @intervention.destroy
    respond_to do |format|
      format.html { redirect_to intervention_url, notice: "L'intervention a été supprimée." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_intervention
      @intervention = Intervention.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def intervention_params
      params.require(:intervention).permit(:project_id, :intervention_date, :duration, :accounted, :details, :comment, :billable, :state, :partner_id)
    end

    def get_partners
      @partners = Partner.order(:name)
    end
end
