json.categories do
  json.array!(@categories) do |category|
    json.extract! category, :id, :title, :sale_vat_id, :purchase_vat_id
    json.url category_url(category, format: :json)
  end
end
