json.intervention do
	json.array!(@interventions) do |intervention|
		  json.extract! intervention, :id, :partner_id, :intervention_date, :request_date, :accounted, :billable, :expected_date, :comment, :canceled, :state, :request, :details, :comment, :user_id
		  json.url intervention_url(intervention, format: :json)
	end
end
