json.projects do
	json.array!(@projects) do |project|
	  json.extract! project, :id, :partner_id, :title, :echance, :facturable, :description, :max_hours
	  json.url project_url(project, format: :json)
	end
end
