json.partners do
  json.array!(@partners) do |partner|
    json.extract! partner, :id, :name, :email, :tel, :mobile, :fax, :address, :lat, :long, :notes, :created_at, :updated_at, :supplier, :customer
    json.logo_medium asset_url(partner.logo.url(:medium))
    json.logo_thumb asset_url(partner.logo.url(:thumb))
    json.url partner_url(partner, format: :json)
  end
end
