json.extract! @partner, :id, :name, :odoo_id, :created_at, :updated_at, :email, :tel, :mobile, :fax, :address, :lat, :long, :notes, :supplier, :customer
json.logo_medium asset_url(@partner.logo.url(:medium))
json.logo_thumb asset_url(@partner.logo.url(:thumb))
