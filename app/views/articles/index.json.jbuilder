json.articles do
  json.array!(@articles) do |article|
    json.extract! article, :id, :purchase_price, :cost_price, :public_price, :supplier_id, :supplier_stock, :category_id, :gencode, :description, :title, :can_sell, :can_purchase, :ref, :warranty, :purchase_vat_id, :sale_vat_id
    json.url article_url(article, format: :json)
    json.supplier article.supplier && article.supplier.name
    json.category article.category && article.category.title
    json.sale_vat article.sale_vat && article.sale_vat.rate
    json.purchase_vat article.purchase_vat && article.purchase_vat.rate
    json.picture_medium asset_url(article.picture.url(:medium))
    json.picture_thumb asset_url(article.picture.url(:thumb))
  end
end
