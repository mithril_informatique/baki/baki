json.extract! @article, :id, :purchase_price, :cost_price, :public_price, :supplier_id,  :category_id, :gencode,:supplier_stock, :description, :title, :can_sell, :can_purchase, :ref, :warranty, :purchase_vat_id, :sale_vat_id, :picture, :created_at, :updated_at
json.supplier @article.supplier && @article.supplier.name
json.category @article.category && @article.category.title
json.sale_vat @article.sale_vat && @article.sale_vat.rate
json.purchase_vat @article.purchase_vat && @article.purchase_vat.rate
json.picture_medium asset_url(@article.picture.url(:medium))
json.picture_thumb asset_url(@article.picture.url(:thumb))
