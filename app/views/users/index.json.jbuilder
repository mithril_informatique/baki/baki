json.user do
	json.array!(@users) do |user|
	  json.extract! user, :id, :email, :crypted_password, :password_salt, :last_login_at, :current_login_at, :current_login_ip, :last_login_ip, :login_count, :failed_login_count, :last_request_at, :name
	  json.url user_url(user, format: :json)
	end
end
