json.intervention do
	json.array!(@interventions) do |intervention|
	  json.extract! intervention, :id, :customer_id, :start_date, :end_date, :compta, :description, :comment, :user_id
	  json.url intervention_url(intervention, format: :json)
	end
end
