json.intervention do
  json.array!(@interventions) do |intervention|
    json.extract! intervention, :id,  :duration, :details, :comment, :created_at, :updated_at, :billable, :accounted, :user_id, :project_id, :user_id, :state, :request_date, :intervention_date, :expected_date
    json.user_name intervention.user.name
    json.partner_id intervention.partner.id
    json.partner_name intervention.partner.name
    json.url intervention_url(intervention, format: :json)
		users = []
		intervention.users do |user|
			users << {id: user.id, name: user.name}
		end
		json.users users
		articles = []
		intervention.interventions_lines do |article|
			articles << {id: article.id, quantity: article.quantity, unit_price: article.unit_price, sale_vat_id: article.sale_vat_id}
		end
		json.articles articles
  end
end
