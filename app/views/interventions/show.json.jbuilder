json.extract! @intervention, :id, :intervention_date, :duration, :details, :comment, :created_at, :updated_at, :billable, :state, :accounted, :project_id, :request_date, :expected_date, :request
json.partner_id @intervention.partner.id
json.partner_name @intervention.partner.name
json.project_name @intervention.project && @intervention.project.title
users = []
@intervention.users do |user|
	users << {id: user.id, name: user.name}
end
json.users = users
articles = []
@intervention.interventions_lines do |article|
	articles << {id: article.id, quantity: article.quantity, unit_price: article.unit_price, sale_vat_id: article.sale_vat_id}
end
json.articles articles
