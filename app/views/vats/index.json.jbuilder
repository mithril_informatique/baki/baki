json.vats do
  json.array!(@vats) do |vat|
    json.extract! vat, :id, :title, :rate, :npr
    json.url vat_url(vat, format: :json)
  end
end
