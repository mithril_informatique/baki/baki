json.projects do
  json.array!(@projects) do |project|
    json.extract! project, :id, :title, :partner_id, :echance, :description, :full_title, :facturable, :max_hours, :duree_interventions
    json.partner_name project.partner.name
    json.url project_url(project, format: :json)
  end
end
