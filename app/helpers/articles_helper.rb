module ArticlesHelper

  def import_articles supplier_name
    fournisseur = Customer.where(name: supplier_name).first
    lignes = SmarterCSV.process(Rails.root.join('tmp','import_articles.csv'), {:col_sep => ";"})
    categories = []
    articles = []
    category_id = nil
    tva = nil
    lignes.each do |ligne|
    	if ligne[:prix].nil? and ligne[:la_mare].nil? and !ligne[:référence].include?("CONCEPT ONE PRODUCTION")
    		category = ligne[:référence].gsub("(TVA)","").gsub("(NP)","").rstrip
    		tva = ligne[:référence].include?("(TVA)") ? 1 : 2
    		categories << {title: category, vat: tva}
    		c = Category.where(title: category).first
        if c
          category_id = c.id
        else
          c = Category.create(title: category, purchase_vat_id: tva, sale_vat_id: tva)
          category_id = c.id
        end
    	else
        if ligne[:prix] and ligne[:désignation] and ligne[:référence]
          prix = ligne[:prix].gsub("€","").to_f
          stock1 = ligne[:la_mare].to_i
          stock2 = ligne[:saint_louis].to_i
          stock3 = ligne[:saint_pierre].to_i
          stock_total = stock1 + stock2 + stock3
          a = Article.where(ref: ligne[:référence]).first
          if prix>0.0 and stock_total>0
            a = Article.new unless a
            a.title = ligne[:désignation]
            a.ref = ligne[:référence]
            a.purchase_price = prix
            a.cost_price = prix
            a.public_price = (prix*1.3).round(2)
            a.category_id = category_id
            a.supplier_id = fournisseur.id
            a.warranty = 24
            a.can_sell = true
            a.can_purchase = true
            a.supplier_stock = stock_total
            a.purchase_vat_id = tva
            a.sale_vat_id = tva
            a.save
          elsif a
            a.destroy
          end
        end
      end
    end
    return 1
  end

end
