function change_picture() {
  $('#picture_field').click();
};

function prepare_articles() {
  pa = $('#pa').val();
  pr = $('#pr').val();
  pv = $('#pv').val();
  frais = pr-pa
  marge = pv-pr
  marge_percent = pr*100/pv;

  $('#pa').on('input', function() {
    pa = $('#pa').val();
    pr = $('#pr').val();
    frais = pr-pa;
    $('#frais').val(frais);
  });

  $('#pr').on('input', function() {
    pa = $('#pa').val();
    pr = $('#pr').val();
    pv = $('#pv').val();
    frais = pr-pa;
    $('#frais').val(frais);
    marge = pv-pr;
    $('#marge').val(marge);
    marge_percent = ((pv-pr)*100/pr).toFixed(2);
    $('#marge_percent').val(marge_percent);
  });

  $('#marge').on('input', function() {
    marge = parseFloat($('#marge').val());
    pr = parseFloat($('#pr').val());
    pv = pr+marge;
    $('#pv').val(pv);
    marge_percent = ((pv-pr)*100/pr).toFixed(2);
    $('#marge_percent').val(marge_percent);
  });

  $('#marge_percent').on('input', function() {
    marge_percent = parseFloat($('#marge_percent').val());
    pr = parseFloat($('#pr').val());
    marge = parseFloat((pr*marge_percent/100).toFixed(2));
    $('#marge').val(marge);
    pv = pr+marge;
    $('#pv').val(pv);
  });

  $('#pv').on('input', function() {
    pr = parseFloat($('#pr').val());
    pv = parseFloat($('#pv').val());
    marge = pv-pr;
    marge_percent = ((pv-pr)*100/pr).toFixed(2);
    $('#marge').val(marge);
    $('#marge_percent').val(marge_percent);
  });
};
