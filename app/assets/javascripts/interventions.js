function change_facturable(e) {
  var project_id = e.value;
  var check = $('#intervention_billable');
  jQuery.ajax({  type: "GET",
                 url: "/projects/" + project_id,
                 dataType: "json",
                 data: {},
                 success: function (data) {
                      check.prop( "checked", data["billable"] );
                 },
                 error : function(resultat, statut, erreur){
                      toastr.error("Impossible de savoir si le projet sélectionné est facturable ou pas\n#{erreur}");
                }
              });
};

function populate_projects(e) {
	var partner_id = e.value;
	var select_project = document.getElementById('intervention_project_id');
	for (i = 0; i <= select_project.options.length; i++) {
  	select_project.options[0] = null;
	}
	jQuery.ajax({  type: "GET",
                 url: "/partners/" + partner_id + "/projects.json",
                 dataType: "json",
                 data: {},
                 success: function (data) {
									 for(i in data) {
										 var option = document.createElement('option');
										 option.value = data[i].id;
										 option.text = data[i].title;
										 select_project.appendChild(option);
									 }
                 },
                 error : function(resultat, statut, erreur){
                      toastr.error("Erreur");
                }
              });
};
