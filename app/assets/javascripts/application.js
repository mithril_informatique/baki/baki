// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require bootstrap-table
//= require bootstrap/bootstrap-tooltip
//= require locale/bootstrap-table-fr-FR
//= require toastr
//= require bootstrap-datepicker
//= require moment
//= require bootstrap-datetimepicker
//= require moment/fr
//= require highcharts
//= require chartkick
//= require bootstrap-switch
//= require articles

$.fn.bootstrapSwitch.defaults.onText = 'Oui';
$.fn.bootstrapSwitch.defaults.offText = 'Non';
$.fn.bootstrapSwitch.defaults.onColor = 'primary';

$(document).ready(function(){
  $('#table').bootstrapTable();
  $('#table2').bootstrapTable();
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = this.href.split('#');
    $('.nav a').filter('[href="#'+target[1]+'"]').tab('show');
  });
  $('.datepicker').datepicker({
    todayBtn: "linked",
    language: "fr",
    todayHighlight: true
  });
  $('.datetimepicker').datetimepicker({
    locale: 'fr'
  });
  $('[data-toggle="tooltip"]').tooltip();
  $('.my-checkbox').bootstrapSwitch();
  $('.my-checkbox').on('switchChange.bootstrapSwitch', function(event, state) {
    this.value = state;
  });
  prepare_articles();
});

function comptabilise(id) {
  var i = $('#buttonComptabilise'+id);
  var bool = false;
  if (i.hasClass("fa-money")) {
    i.removeClass("fa-money").addClass("fa-spin fa-spinner");
    bool = true;
  } else {
    i.removeClass("fa-check").addClass("fa-spin fa-spinner");
    bool = false;
  }

  jQuery.ajax({  type: "PATCH",
                 url: "/interventions/" + id,
                 dataType: "json",
                 data: {intervention: {compta: bool}},
                 success: function (data) {
                      classe = data["compta"] ? "fa-check" : "fa-money"
                      message = data["compta"] ? "L'intervention a été marqué comme comptabilisée" : "L'intervention n'a plus l'étiquette comptabilisée"
                      toastr.success(message);
                      i.removeClass('fa-spin fa-spinner').addClass(classe);
                 },
                 error : function(resultat, statut, erreur){
                      classe = bool ? "fa-money" : "fa-check"
                      toastr.error("La mise à jour du module contact est un échec");
                      i.removeClass('fa-spin fa-spinner').addClass(classe);
                }
              });
};
