class AddRequestToInterventions < ActiveRecord::Migration
  def change
    rename_column :interventions, :description, :details
    rename_column :interventions, :facturable, :billable
    rename_column :interventions, :start_date, :intervention_date
    add_column :interventions, :request, :string

    add_column :interventions, :request_date, :date
    add_column :interventions, :expected_date, :date
    add_column :interventions, :canceled, :boolean

  end
end
