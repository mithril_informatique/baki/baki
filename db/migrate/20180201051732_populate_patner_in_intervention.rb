class PopulatePatnerInIntervention < ActiveRecord::Migration
  def change
		Intervention.all.each do |intervention|
			if !intervention.partner_id and intervention.project_id
				intervention.partner_id = intervention.project.partner_id
				intervention.save
			end
		end
  end
end
