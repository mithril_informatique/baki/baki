class RenameCustomerId < ActiveRecord::Migration
  def change
		rename_column :projects, :customer_id, :partner_id
  end
end
