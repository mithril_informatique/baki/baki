class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.float :purchase_price
      t.float :cost_price
      t.float :public_price
      t.references :supplier
      t.string :serial_number
      t.references :category, index: true, foreign_key: true
      t.string :gencode
      t.text :description
      t.string :title
      t.boolean :can_sell
      t.boolean :can_purchase
      t.string :ref
      t.integer :warranty
      t.references :purchase_vat
      t.references :sale_vat
      t.attachment :picture

      t.timestamps null: false
    end
    add_foreign_key :articles, :vats, column: :sale_vat_id, primary_key: :id
    add_foreign_key :articles, :vats, column: :purchase_vat_id, primary_key: :id
    add_foreign_key :articles, :customers, column: :supplier_id, primary_key: :id
  end
end
