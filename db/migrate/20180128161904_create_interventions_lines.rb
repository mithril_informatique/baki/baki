class CreateInterventionsLines < ActiveRecord::Migration
  def change
    create_table :interventions_lines , id:false do |t|
      t.belongs_to :intervention, index: true
      t.belongs_to :article, index: true
      t.integer :quantity
      t.float :quantity
      t.references :sale_vat
    end
    add_foreign_key :interventions_lines, :vats, column: :sale_vat_id, primary_key: :id
  end
end
