class ChangeComptaDefault < ActiveRecord::Migration
  def change
    change_column_default(:sheets, :compta, false)
  end
end
