class CreateInterventionsUsers < ActiveRecord::Migration
 
  def change
    create_table :interventions_users, id:false do |t|
      t.belongs_to :intervention, index: true
      t.belongs_to :user, index: true
    end
  end
end
