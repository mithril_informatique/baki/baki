class AddLogoToCustomer < ActiveRecord::Migration
  def change
    add_attachment :customers, :logo
  end
end
