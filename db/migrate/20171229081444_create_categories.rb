class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :title
      t.references :sale_vat
      t.references :purchase_vat

      t.timestamps null: false
    end
    add_foreign_key :categories, :vats, column: :sale_vat_id, primary_key: :id
    add_foreign_key :categories, :vats, column: :purchase_vat_id, primary_key: :id
  end
end
