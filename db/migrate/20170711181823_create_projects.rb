class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :title
      t.references :customer, index: true, foreign_key: true
      t.date :echance
      t.text :description

      t.timestamps null: false
    end
  end
end
