class AddCustomerToPartners < ActiveRecord::Migration
  def change
    add_column :partners, :customer, :boolean
  end
end
