class AddProjetToSheet < ActiveRecord::Migration
  def change
    add_reference :sheets, :project, index: true, foreign_key: true
    remove_column :sheets, :customer_id
  end
end
