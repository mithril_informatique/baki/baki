class CreateVats < ActiveRecord::Migration
  def change
    create_table :vats do |t|
      t.string :title
      t.float :rate
      t.boolean :npr

      t.timestamps null: false
    end
  end
end
