class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :crypted_password
      t.string :password_salt
      t.datetime :last_login_at
      t.datetime :current_login_at
      t.string :current_login_ip
      t.string :last_login_ip
      t.integer :login_count, default: 0, null: false
      t.integer :failed_login_count, default: 0, null: false
      t.datetime :last_request_at
      t.string :name

      t.timestamps null: false
    end
  end
end
