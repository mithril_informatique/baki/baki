class AddSupplierStockToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :supplier_stock, :integer
    remove_column :articles, :serial_number
    add_column :articles, :trackable, :boolean, default: false
  end
end
