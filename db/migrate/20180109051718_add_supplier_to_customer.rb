class AddSupplierToCustomer < ActiveRecord::Migration
  def change
    add_column :customers, :supplier, :boolean
  end
end
