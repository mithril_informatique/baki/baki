class AddFacturableToSheet < ActiveRecord::Migration
  def change
    add_column :sheets, :facturable, :boolean
    remove_column :sheets, :facturation
  end
end
