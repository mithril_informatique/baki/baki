class AddDurationToSheets < ActiveRecord::Migration
  def change
    add_column :sheets, :duration, :integer
    remove_column :sheets, :end_date
  end
end
