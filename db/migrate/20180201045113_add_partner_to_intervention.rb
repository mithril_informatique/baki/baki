class AddPartnerToIntervention < ActiveRecord::Migration
  def change
    add_reference :interventions, :partner, index: true, foreign_key: true
  end
end
