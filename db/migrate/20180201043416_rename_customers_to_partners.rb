class RenameCustomersToPartners < ActiveRecord::Migration
  def change
		rename_table :customers, :partners
  end
end
