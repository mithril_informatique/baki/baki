class CreateSheets < ActiveRecord::Migration
  def change
    create_table :sheets do |t|
      t.belongs_to :customer, index: true, foreign_key: true
      t.datetime :start_date
      t.datetime :end_date
      t.boolean :compta
      t.text :description
      t.text :comment

      t.timestamps null: false
    end
  end
end
