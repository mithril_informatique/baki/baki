class ChangeMinutesToHours < ActiveRecord::Migration
  def change
    Sheet.all do |sheet|
      sheet.duration = sheet.duration/60
      sheet.save
    end
  end
end
