class AddFacturationToSheet < ActiveRecord::Migration
  def change
    add_column :sheets, :facturation, :integer, default: 100
  end
end
