class AddUnitPriceToInterventionsLine < ActiveRecord::Migration
  def change
    add_column :interventions_lines, :unit_price, :float
		add_column :interventions_lines, :created_at, :datetime, null: false, default:DateTime.now
    add_column :interventions_lines, :updated_at, :datetime, null: false, default:DateTime.now
		add_column :interventions_lines, :id, :primary_key
  end
end
