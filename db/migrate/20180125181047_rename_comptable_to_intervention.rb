class RenameComptableToIntervention < ActiveRecord::Migration
  def change
    rename_column :interventions, :compta, :accounted
  end
end
