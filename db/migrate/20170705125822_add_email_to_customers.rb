class AddEmailToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :email, :string
    add_column :customers, :tel, :string
    add_column :customers, :mobile, :string
    add_column :customers, :fax, :string
    add_column :customers, :address, :text
    add_column :customers, :lat, :float
    add_column :customers, :long, :float
    add_column :customers, :notes, :text
  end
end
