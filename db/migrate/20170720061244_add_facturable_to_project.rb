class AddFacturableToProject < ActiveRecord::Migration
  def change
    add_column :projects, :facturable, :boolean
  end
end
