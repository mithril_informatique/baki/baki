class AddStateToSheet < ActiveRecord::Migration
  def change
    add_column :sheets, :state, :integer, default: 1
    remove_column :sheets, :state_sheet
    
  end
end
