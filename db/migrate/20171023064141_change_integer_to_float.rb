class ChangeIntegerToFloat < ActiveRecord::Migration
  def change
    change_column :projects, :max_hours, :float
    change_column :sheets, :duration, :float
  end
end
