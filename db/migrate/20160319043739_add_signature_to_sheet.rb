class AddSignatureToSheet < ActiveRecord::Migration
  def change
    add_attachment :sheets, :signature
  end
end
