# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180201051732) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "articles", force: :cascade do |t|
    t.float    "purchase_price"
    t.float    "cost_price"
    t.float    "public_price"
    t.integer  "supplier_id"
    t.integer  "category_id"
    t.string   "gencode"
    t.text     "description"
    t.string   "title"
    t.boolean  "can_sell"
    t.boolean  "can_purchase"
    t.string   "ref"
    t.integer  "warranty"
    t.integer  "purchase_vat_id"
    t.integer  "sale_vat_id"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "supplier_stock"
    t.boolean  "trackable",            default: false
  end

  add_index "articles", ["category_id"], name: "index_articles_on_category_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "title"
    t.integer  "sale_vat_id"
    t.integer  "purchase_vat_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "interventions", force: :cascade do |t|
    t.datetime "intervention_date"
    t.boolean  "accounted",              default: false
    t.text     "details"
    t.text     "comment"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "signature_file_name"
    t.string   "signature_content_type"
    t.integer  "signature_file_size"
    t.datetime "signature_updated_at"
    t.integer  "user_id"
    t.float    "duration"
    t.integer  "project_id"
    t.boolean  "billable"
    t.string   "request"
    t.date     "request_date"
    t.date     "expected_date"
    t.boolean  "canceled"
    t.integer  "state",                  default: 1
    t.integer  "partner_id"
  end

  add_index "interventions", ["partner_id"], name: "index_interventions_on_partner_id", using: :btree
  add_index "interventions", ["project_id"], name: "index_sheets_on_project_id", using: :btree
  add_index "interventions", ["user_id"], name: "index_sheets_on_user_id", using: :btree

  create_table "interventions_lines", force: :cascade do |t|
    t.integer  "intervention_id"
    t.integer  "article_id"
    t.float    "quantity"
    t.integer  "sale_vat_id"
    t.float    "unit_price"
    t.datetime "created_at",      default: '2018-02-01 05:33:00', null: false
    t.datetime "updated_at",      default: '2018-02-01 05:33:00', null: false
  end

  add_index "interventions_lines", ["article_id"], name: "index_interventions_lines_on_article_id", using: :btree
  add_index "interventions_lines", ["intervention_id"], name: "index_interventions_lines_on_intervention_id", using: :btree

  create_table "interventions_users", id: false, force: :cascade do |t|
    t.integer "intervention_id"
    t.integer "user_id"
  end

  add_index "interventions_users", ["intervention_id"], name: "index_interventions_users_on_intervention_id", using: :btree
  add_index "interventions_users", ["user_id"], name: "index_interventions_users_on_user_id", using: :btree

  create_table "partners", force: :cascade do |t|
    t.string   "name"
    t.integer  "odoo_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "email"
    t.string   "tel"
    t.string   "mobile"
    t.string   "fax"
    t.text     "address"
    t.float    "lat"
    t.float    "long"
    t.text     "notes"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.boolean  "supplier"
    t.boolean  "customer"
  end

  create_table "projects", force: :cascade do |t|
    t.string   "title"
    t.integer  "partner_id"
    t.date     "echance"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.boolean  "facturable"
    t.float    "max_hours"
  end

  add_index "projects", ["partner_id"], name: "index_projects_on_partner_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.datetime "last_login_at"
    t.datetime "current_login_at"
    t.string   "current_login_ip"
    t.string   "last_login_ip"
    t.integer  "login_count",         default: 0, null: false
    t.integer  "failed_login_count",  default: 0, null: false
    t.datetime "last_request_at"
    t.string   "name"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "persistence_token"
    t.string   "single_access_token"
    t.string   "perishable_token"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  create_table "vats", force: :cascade do |t|
    t.string   "title"
    t.float    "rate"
    t.boolean  "npr"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "articles", "categories"
  add_foreign_key "articles", "partners", column: "supplier_id"
  add_foreign_key "articles", "vats", column: "purchase_vat_id"
  add_foreign_key "articles", "vats", column: "sale_vat_id"
  add_foreign_key "categories", "vats", column: "purchase_vat_id"
  add_foreign_key "categories", "vats", column: "sale_vat_id"
  add_foreign_key "interventions", "partners"
  add_foreign_key "interventions", "projects"
  add_foreign_key "interventions", "users"
  add_foreign_key "interventions_lines", "vats", column: "sale_vat_id"
  add_foreign_key "projects", "partners"
end
